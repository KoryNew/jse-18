package ru.tsk.vkorenygin.tm.exception.user;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class EmailOccupiedException extends AbstractException {

    public EmailOccupiedException(String email) {
        super("Error! User with e-mail '" + email + "' is already registered.");
    }

}
