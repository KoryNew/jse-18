package ru.tsk.vkorenygin.tm.exception.empty;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() {
        super("Error! Role is empty.");
    }

}
