package ru.tsk.vkorenygin.tm.command;

import ru.tsk.vkorenygin.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand{

    public void show(Project project) {
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
        System.out.println("Create date: " + project.getCreateDate());
    }

}
