package ru.tsk.vkorenygin.tm.command.project;

import ru.tsk.vkorenygin.tm.command.AbstractProjectCommand;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.model.Project;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

public class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String description() {
        return "remove project by name";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project projectByName = serviceLocator.getProjectService().findByName(name);
        if (projectByName == null) throw new ProjectNotFoundException();
        final Project project = serviceLocator.getProjectTaskService().removeProjectById(projectByName.getId());
        if (project == null) throw new ProjectNotFoundException();
    }

}

