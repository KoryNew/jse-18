package ru.tsk.vkorenygin.tm.command.project;

import ru.tsk.vkorenygin.tm.command.AbstractProjectCommand;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "remove all projects";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        serviceLocator.getProjectService().clear();
    }

}
