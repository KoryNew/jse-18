package ru.tsk.vkorenygin.tm.command.task;

import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;
import ru.tsk.vkorenygin.tm.model.Task;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

public class TaskFinishByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Override
    public String description() {
        return "finish task by name";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

}
