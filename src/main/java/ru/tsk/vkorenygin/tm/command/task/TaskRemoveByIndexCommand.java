package ru.tsk.vkorenygin.tm.command.task;

import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;
import ru.tsk.vkorenygin.tm.model.Task;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    public String description() {
        return "remove task by index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().removeByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

}
