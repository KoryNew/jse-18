package ru.tsk.vkorenygin.tm.command.system;

import ru.tsk.vkorenygin.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String description() {
        return "display program version";
    }

    @Override
    public void execute() {
        System.out.println("- VERSION -");
        System.out.println("1.18.1");
    }

}
