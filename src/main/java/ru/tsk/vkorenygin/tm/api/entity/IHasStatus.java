package ru.tsk.vkorenygin.tm.api.entity;

import ru.tsk.vkorenygin.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
