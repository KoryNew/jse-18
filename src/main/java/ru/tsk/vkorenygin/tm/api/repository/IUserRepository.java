package ru.tsk.vkorenygin.tm.api.repository;

import ru.tsk.vkorenygin.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User remove(User user);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeById(String id);

    User removeByLogin(String login);

}
