package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.user.AccessDeniedException;
import ru.tsk.vkorenygin.tm.model.User;

public interface IAuthService {

    User getUser() throws AbstractException;

    String getUserId() throws AccessDeniedException;

    boolean isAuth();

    void logout();

    void login(String login, String password) throws AbstractException;

    void registry(String login, String password, String email) throws AbstractException;

}
