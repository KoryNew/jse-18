package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.api.service.IProjectTaskService;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.model.Project;
import ru.tsk.vkorenygin.tm.model.Task;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String projectId, final String taskId) {
        if (DataUtil.isEmpty(projectId))
            throw new EmptyIdException();
        if (DataUtil.isEmpty(taskId))
            throw new EmptyIdException();
        if (!projectRepository.existsById(projectId))
            throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId))
            throw new TaskNotFoundException();
        return taskRepository.bindTaskToProjectById(projectId, taskId);
    }

    @Override
    public Task unbindTaskFromProject(final String projectId, final String taskId) {
        if (DataUtil.isEmpty(projectId))
            throw new EmptyIdException();
        if (DataUtil.isEmpty(taskId))
            throw new EmptyIdException();
        if (!projectRepository.existsById(projectId))
            throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId))
            throw new TaskNotFoundException();
        return taskRepository.unbindTaskById(taskId);
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String id) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        if (!projectRepository.existsById(id))
            throw new ProjectNotFoundException();
        return taskRepository.findAllByProjectId(id);
    }

    @Override
    public Project removeProjectById(final String id) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        if (!projectRepository.existsById(id))
            throw new ProjectNotFoundException();
        taskRepository.removeAllTaskByProjectId(id);
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeProjectByIndex(final int index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        final Project project = projectRepository.findByIndex(index);
        if (project == null)
            throw new ProjectNotFoundException();
        taskRepository.removeAllTaskByProjectId(project.getId());
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeProjectByName(final String name) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        final Project project = projectRepository.findByName(name);
        if (project == null)
            throw new ProjectNotFoundException();
        taskRepository.removeAllTaskByProjectId(project.getId());
        return projectRepository.removeByName(name);
    }

}
