package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.api.service.IProjectService;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyDescriptionException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.model.Project;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(String name) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(project);
    }

    @Override
    public void create(String name, String description) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        if (DataUtil.isEmpty(description))
            throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(Project project) {
        if (project == null)
            throw new ProjectNotFoundException();
        projectRepository.add(project);
    }

    @Override
    public boolean existsById(final String id) {
        if (DataUtil.isEmpty(id))
            return false;
        return projectRepository.existsById(id);
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        if (DataUtil.isEmpty(index))
            return false;
        return projectRepository.existsByIndex(index);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null)
            return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project findById(final String id) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return projectRepository.findById(id);
    }

    @Override
    public Project findByName(final String name) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return projectRepository.findByName(name);
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        if (status == null)
            return null;
        return projectRepository.changeStatusById(id, status);
    }

    @Override
    public Project changeStatusByName(final String name, final Status status) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        if (status == null)
            return null;
        return projectRepository.changeStatusByName(name, status);
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        if (status == null)
            return null;
        return projectRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Project startById(String id) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return projectRepository.startById(id);
    }

    @Override
    public Project startByIndex(Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(String name) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return projectRepository.startByName(name);
    }

    @Override
    public Project finishById(String id) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return projectRepository.finishById(id);
    }

    @Override
    public Project finishByIndex(Integer index) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(String name) {
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return projectRepository.finishByName(name);
    }

    @Override
    public Project updateByIndex(final Integer index, final String name, final String description) {
        if (DataUtil.isEmpty(index))
            throw new IncorrectIndexException();
        if (index-1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        final Project project = projectRepository.findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateById(final String id, final String name, final String description) {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        final Project project = projectRepository.findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public void remove(Project project) {
        if (project == null)
            throw new ProjectNotFoundException();
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

}
