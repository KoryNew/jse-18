package ru.tsk.vkorenygin.tm.service;

import ru.tsk.vkorenygin.tm.api.repository.IUserRepository;
import ru.tsk.vkorenygin.tm.api.service.IUserService;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyEmailException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyLoginException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyPasswordException;
import ru.tsk.vkorenygin.tm.exception.user.EmailOccupiedException;
import ru.tsk.vkorenygin.tm.exception.user.LoginExistsException;
import ru.tsk.vkorenygin.tm.model.User;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.List;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public boolean isLoginExists(final String login) throws EmptyLoginException {
        if (DataUtil.isEmpty(login)) return false;
        return (findByLogin(login) != null);
    }

    @Override
    public boolean isEmailExists(final String email) throws EmptyEmailException {
        if (DataUtil.isEmpty(email)) return false;
        return (findByEmail(email) != null);
    }

    @Override
    public User create(final String login, final String password) throws AbstractException {
        if (DataUtil.isEmpty(login))
            throw new EmptyLoginException();
        if (DataUtil.isEmpty(password))
            throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) throws AbstractException {
        if (DataUtil.isEmpty(login))
            throw new EmptyLoginException();
        if (DataUtil.isEmpty(password))
            throw new EmptyPasswordException();
        if (DataUtil.isEmpty(email))
            throw new EmptyEmailException();
        if (isLoginExists(login))
            throw new LoginExistsException(login);
        if (isEmailExists(email))
            throw new EmailOccupiedException(email);
        final User user = create(login, password);
        if (user == null)
            return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) throws AbstractException {
        if (DataUtil.isEmpty(login))
            throw new EmptyLoginException();
        if (DataUtil.isEmpty(password))
            throw new EmptyPasswordException();
        if (isLoginExists(login))
            throw new LoginExistsException(login);
        if (role == null)
            throw new EmptyEmailException();
        final User user = create(login, password);
        if (user == null)
            return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User remove(final User user) {
        if (user == null) return null;
        return userRepository.remove(user);
    }

    @Override
    public User findById(final String id)
            throws EmptyIdException {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) throws EmptyLoginException {
        if (DataUtil.isEmpty(login))
            throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) throws EmptyEmailException {
        if (DataUtil.isEmpty(email))
            throw new EmptyEmailException();
        return userRepository.findByLogin(email);
    }

    @Override
    public User removeById(final String id) throws EmptyIdException {
        if (DataUtil.isEmpty(id))
            throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) throws EmptyLoginException {
        if (DataUtil.isEmpty(login))
            throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User setPassword(final String userId, final String password) throws AbstractException{
        if (DataUtil.isEmpty(userId))
            throw new EmptyIdException();
        if (DataUtil.isEmpty(password))
            throw new EmptyPasswordException();
        final User user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
        return user;
    }

    @Override
    public User updateUser(
        final String userId,
        final String firstName,
        final String lastName,
        final String middleName
    ) throws EmptyIdException {
        if (DataUtil.isEmpty(userId))
            throw new EmptyIdException();
        final User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

}
